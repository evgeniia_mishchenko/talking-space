<?php require('core/init.php'); ?>

<?php
    //Create Topic Object
    $topic = new Topic;
    $user = new User;

    //Get Template & Assign Vars
    $template = new Template('templates/frontpage.php');

    //SO our index file is nice and clean
    // all we have is template object which we output..But not only that - we can pass values:
    //Assign Vars
    //$template->heading = 'this is heading';
    //so such we can pass things from our Controller (index.php/login.php/register.php/topic.php.. are basically our controller)
    // we fetch data from the library which is our Model

    //Assign Vars
    $template->topics = $topic->getAllTopics();
    //For statistic area
    $template->totalTopics = $topic->getTotalTopics();
    $template->totalCategories = $topic->getTotalCategories();
    $template->totalUsers = $user->getTotalUsers();    //later will be inside of User class


//Display template
    echo $template;



