<?php include 'includes/header.php'; ?>



                    <div class="row">

                        <form class="form-horizontal" role="form" method="POST" action="">

                            <div class="form-group">
                                <div class="col-md-1"></div>
                                <label for="topic" class="col-md-3">Topic Title <span class="required">*</span></label>
                                <div class="col-md-7">
                                    <input type="text" name="title" class="form-control" id="topic" placeholder="Enter Post Title">
                                </div>
                                <div class="col-md-1"></div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-1"></div>
                                <label for="category" class="col-md-3">Category <span class="required">*</span></label>
                                <div class="col-md-7">
                                    <select class="form-control" id="category" name="category">
                                    <?php foreach(getCategories() as $category) : ?>
                                        <option value="<?php echo $category->id; ?>"><?php echo $category->name; ?></option>
                                    <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-1"></div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-1"></div>
                                <label for="body" class="col-md-3">Topic Body <span class="required">*</span></label>
                                <div class="col-md-7">
                                    <textarea name="body" class="form-control" id="body" cols="80" rows="5" placeholder="Enter Topic Body"></textarea>
                                    <input type="submit" id="create" name="do_create" class="btn btn-primary" value="Create">

                                </div>
                                <div class="col-md-1"></div>
                            </div>

                        </form>
                    </div>


<?php include 'includes/footer.php'; ?>
