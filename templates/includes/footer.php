</div>


</div>
</div>
<div class="col-md-1"></div>
<div class="col-md-3">
    <div id="sidebar">

        <div class="block">
            <h4> Login Form </h4>
            <hr>
            <!-- We want form not to show but instead logout button when user is logged in
                Instead of using if session var we will use a cleaner helper function-->
            <?php if(isLoggedIn()) : ?>
                <div class="userdata">
                    Welcome, <?php echo getUser()['username']; /*function in system_helper*/?> !
                </div>

                <form role="form" method="POST" action="logout.php">
                    <input type="submit" name="do_logout" class="btn btn-primary" value="Logout" />
                </form>
            <?php else : ?>
            <form id="login" role="form" method="POST" action="login.php">
                <div class="form-group">
                    <label for="name" class="sr-only">Username: </label>
                    <input type="text" name="username" id="name" class="form-control" placeholder="Enter your name">
                    <span class="glyphicon glyphicon-user form-control-feedback right" aria-hidden="true"></span>
                </div>
                <div class="form-group">
                    <label for="pass" class="sr-only">Password: </label>
                    <input type="password" name="password" class="form-control" id="pass" placeholder="Enter your password">
                    <span class="glyphicon glyphicon-lock form-control-feedback  right" aria-hidden="true"></span>
                </div>
                <div class="buttons-group">
                    <input type="submit" name="do_login" class="btn btn-primary" value="Login" />  <a class="btn btn-default" href="register.php" role="button">Create Account</a>
                </div>
            </form>
            <?php endif; ?>
        </div>

        <div class="block">
            <h4> Categories </h4>
            <hr>

            <ul class="nav nav-pills nav-stacked">
                <?php foreach(getCategories() as $category) : ?>
                    <li class="<?php echo is_active($category->id); ?>" role="presentation"><a href="topics.php?category=<?php echo $category->id; ?>"><?php echo $category->name; ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>
</div>

</div><!-- /.container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="<?php echo BASE_URI; ?>templates/js/bootstrap.js"></script>
<!-- Register page - script for Upload file custom input-->
<script>
    !function(e){var t=function(t,n){this.$element=e(t),this.type=this.$element.data("uploadtype")||(this.$element.find(".thumbnail").length>0?"image":"file"),this.$input=this.$element.find(":file");if(this.$input.length===0)return;this.name=this.$input.attr("name")||n.name,this.$hidden=this.$element.find('input[type=hidden][name="'+this.name+'"]'),this.$hidden.length===0&&(this.$hidden=e('<input type="hidden" />'),this.$element.prepend(this.$hidden)),this.$preview=this.$element.find(".fileupload-preview");var r=this.$preview.css("height");this.$preview.css("display")!="inline"&&r!="0px"&&r!="none"&&this.$preview.css("line-height",r),this.original={exists:this.$element.hasClass("fileupload-exists"),preview:this.$preview.html(),hiddenVal:this.$hidden.val()},this.$remove=this.$element.find('[data-dismiss="fileupload"]'),this.$element.find('[data-trigger="fileupload"]').on("click.fileupload",e.proxy(this.trigger,this)),this.listen()};t.prototype={listen:function(){this.$input.on("change.fileupload",e.proxy(this.change,this)),e(this.$input[0].form).on("reset.fileupload",e.proxy(this.reset,this)),this.$remove&&this.$remove.on("click.fileupload",e.proxy(this.clear,this))},change:function(e,t){if(t==="clear")return;var n=e.target.files!==undefined?e.target.files[0]:e.target.value?{name:e.target.value.replace(/^.+\\/,"")}:null;if(!n){this.clear();return}this.$hidden.val(""),this.$hidden.attr("name",""),this.$input.attr("name",this.name);if(this.type==="image"&&this.$preview.length>0&&(typeof n.type!="undefined"?n.type.match("image.*"):n.name.match(/\.(gif|png|jpe?g)$/i))&&typeof FileReader!="undefined"){var r=new FileReader,i=this.$preview,s=this.$element;r.onload=function(e){i.html('<img src="'+e.target.result+'" '+(i.css("max-height")!="none"?'style="max-height: '+i.css("max-height")+';"':"")+" />"),s.addClass("fileupload-exists").removeClass("fileupload-new")},r.readAsDataURL(n)}else this.$preview.text(n.name),this.$element.addClass("fileupload-exists").removeClass("fileupload-new")},clear:function(e){this.$hidden.val(""),this.$hidden.attr("name",this.name),this.$input.attr("name","");if(navigator.userAgent.match(/msie/i)){var t=this.$input.clone(!0);this.$input.after(t),this.$input.remove(),this.$input=t}else this.$input.val("");this.$preview.html(""),this.$element.addClass("fileupload-new").removeClass("fileupload-exists"),e&&(this.$input.trigger("change",["clear"]),e.preventDefault())},reset:function(e){this.clear(),this.$hidden.val(this.original.hiddenVal),this.$preview.html(this.original.preview),this.original.exists?this.$element.addClass("fileupload-exists").removeClass("fileupload-new"):this.$element.addClass("fileupload-new").removeClass("fileupload-exists")},trigger:function(e){this.$input.trigger("click"),e.preventDefault()}},e.fn.fileupload=function(n){return this.each(function(){var r=e(this),i=r.data("fileupload");i||r.data("fileupload",i=new t(this,n)),typeof n=="string"&&i[n]()})},e.fn.fileupload.Constructor=t,e(document).on("click.fileupload.data-api",'[data-provides="fileupload"]',function(t){var n=e(this);if(n.data("fileupload"))return;n.fileupload(n.data());var r=e(t.target).closest('[data-dismiss="fileupload"],[data-trigger="fileupload"]');r.length>0&&(r.trigger("click.fileupload"),t.preventDefault())})}(window.jQuery)
</script>
</body>
</html>