<?php include 'includes/header.php'; ?>



                    <?php if($topics) : ?>
                        <?php foreach($topics as $topic) : ?>
                            <div class="media" id="topics">
                                <div class="media-left">
                                    <img class="media-object" id="user_avatar" src="images/avatars/<?php echo $topic->avatar; ?>" alt="user's aravatar">
                                    <ul>
                                        <li class="text-center"><strong><a href="#"><?php echo $topic->username; ?></a></strong></li>
                                        <li class="text-center">
                                            <a href="topics.php?user=<?php echo $topic->user_id; ?>">
                                                <?php echo userPostCount($topic->user_id); ?> Posts
                                            </a>
                                        </li>
                                        <li class="text-center"><?php echo userReplyCount($topic->user_id); ?> Replies</li>
                                    </ul>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading"><a href="topic.php?id=<?php echo $topic->id; ?>"><?php echo $topic->title; ?></a></h4>
                                    <p><i>Posted on: <?php echo formatDate($topic->create_date); ?></i></p>
                                    <div class="topic-info">
                                        <p><?php echo $topic->body; ?></p>
                                        <i>Category: </i><a href="topics.php?category=<?php echo urlFormat($topic->category_id); ?>"><?php echo $topic->name; ?></a>
                                        <span class="badge pull-right"><?php echo replyCount($topic->id); ?> Replies</span>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <br>
                        <p>No Topics To Display</p>
                        <br>
                    <?php endif; ?>



                    <h5><strong>Forum Statistics</strong></h5>
                    <ul class="nav nav-pills nav-stacked">
                        <li><p>Total Number of Users: <span class="badge"><?php echo $totalUsers; ?></span></p></li>
                        <li><p>Total Number of Topics: <span class="badge"><?php echo $totalTopics; ?></span></p></li>
                        <li><p>Total Number of Gategories: <span class="badge"><?php echo $totalCategories; ?></span></p></li>
                    </ul>
<?php include 'includes/footer.php'; ?>
