<?php include 'includes/header.php'; ?>

                </div>
                <div class="block">
                    <div class="media" id="topics">
                        <div class="media-left">
                                <img class="media-object" id="user_avatar" src="<?php echo BASE_URI; ?>images/avatars/<?php echo $topic->avatar; ?>" alt="user's aravatar">
                                <ul>
                                    <li class="text-center"><strong><a href="#"><?php echo $topic->username; ?></a></strong></li>
                                    <li class="text-center">
                                        <a href="topics.php?user=<?php echo $topic->user_id; ?>">
                                            <?php echo userPostCount($topic->user_id); ?> Posts
                                        </a>
                                    </li>
                                    <li class="text-center"><?php echo userReplyCount($topic->user_id); ?> Replies</li>
                                </ul>

                        </div>
                        <div class="media-body">
                            <div class="topic-info">
                                <div style="margin-bottom:20px;">
                                    <p class="text-right"><i>Posted on: <?php echo formatDate($topic->create_date); ?></i></p>
                                    <p><?php echo $topic->body; ?></p>
                                </div>
                                <hr>
                                <i>Category: </i><a href="topics.php?category=<?php echo urlFormat($topic->category_id); ?>"><?php echo $topic->name; ?></a>
                                <span class="badge pull-right">3 Replies</span></p>
                            </div>
                        </div>
                    </div>
                </div>

                <?php foreach($replies as $reply) : ?>
                <div class="block">
                    <div class="media" id="topics">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object" id="user_avatar"  src="<?php echo BASE_URI; ?>images/avatars/<?php echo $reply->avatar; ?>" alt="user's aravatar">
                            </a>
                            <ul>
                                <li class="text-center"><strong><a href="#"><?php echo $reply->username; ?></a></strong></li>
                                <li class="text-center">
                                    <a href="topics.php?user=<?php echo $reply->user_id; ?>">
                                        <?php echo userPostCount($reply->user_id); ?> Posts
                                    </a>
                                </li>
                                <li class="text-center"><?php echo userReplyCount($reply->user_id); ?> Replies</li>
                            </ul>
                        </div>
                        <div class="media-body">
                            <p>Posted on: <?php echo formatDate($reply->create_date); ?></p>
                            <div class="topic-info">
                                <p><?php echo $reply->body; ?></p>
                                <a href="register.html">Reply</a> | <a href="register.html">Like</a> |  234 <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span><span class="badge pull-right">3 Replies</span>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
                <?php endforeach; ?>

                <div class="block">
                    <h3>Reply To Topic</h3>
                    <hr>
                    <?php if(isLoggedIn()) : ?>
                        <form role="form" method="POST" action="topic.php?id=<?php echo $topic->id; ?>">
                            <div class="form-group" >
                                <textarea class="form-control" name="body" id="reply" rows="10" cols="80"></textarea>
                            </div>
                            <input type="submit" name="do_reply" class="btn btn-default" value="Send">
                        </form>
                    <?php else : ?>
                        <p> Please login to reply </p>
                    <?php endif; ?>



<?php include 'includes/footer.php'; ?>
