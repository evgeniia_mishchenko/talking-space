<!--this is not gonna be a displayed file, user not gonna go to this login page
    its just handle our login - we send our form to it and then it validate it against DB
    through the User class and then redirect us
    -->

<?php include('core/init.php'); ?>  <!--first include our initializer-->

<?php
    if(isset($_POST['do_login'])){      //it is a submit button name
        //Get Vars
        $username = $_POST['username'];
        $password = md5($_POST['password']);    //without md5 its gonna match nonencrypted password against encrypted password

        //Create User Object
        $user = new User;

        if($user->login($username, $password)){

            redirect('index.php','You have been logged in','success');
        } else {
            redirect('index.php','That login is not valid ','error');
        }
    } else {    //id submit button wasnt clicked when someone just navigate to login.php
        redirect('index.php');
    }

