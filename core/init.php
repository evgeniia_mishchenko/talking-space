<?php
//Star Session
session_start();
//when u use sessions u need to strat it on every page


//here we gonna create our autoloader of our classes
//as well as any requires that we need

//first require our config file that it is available to us

//Include Configurations
require_once('config/config.php');
//Helper Function Files
require_once('helpers/system_helper.php');
require_once('helpers/format_helper.php');
require_once('helpers/db_helper.php');


//Autoloader Classes
function __autoload($class_name){
    require_once('libraries/'.$class_name.'.php');
}
//its gonna let us create any classes we want in our libraries
//and as long as the file name matches the class name we dont have require our classes,
//they automatically be required due this autoloader. that is really important because
//u dont have a million requires() or includes().
//but here is newer function - spl_autoload_register()
