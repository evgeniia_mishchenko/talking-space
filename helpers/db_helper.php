<?php
/*
 * Get number of replies per topic
 */
    function replyCount($topic_id){
        $db = new Database;
        $db->query('SELECT * FROM replies WHERE topic_id = :topic_id');
        $db->bind(':topic_id', $topic_id);
        //Assign Rows
        $rows = $db->resultset();
        //Get count
        return $db->rowCount(); //we dont need actual rows to use, we only need the count
    }
/*
 *Get Categories
 */
    function getCategories(){
    $db = new Database;
    $db->query('SELECT * FROM categories');
    //Assign Result Set
    $results = $db->resultset();
    return $results;
    }

/*
 * User Posts
 */
    function userPostCount ($user_id){
        $db = new Database;
        $db->query('SELECT * FROM topics WHERE user_id = :user_id');
        $db->bind(':user_id', $user_id);
        $rows = $db->resultset();
        $topic_count = $db->rowCount();
        return $topic_count;
    }

/*
 * User Replies
 */
    function userReplyCount ($user_id){
        $db = new Database;
        $db->query('SELECT * FROM replies WHERE user_id = :user_id');
        $db->bind(':user_id', $user_id);
        $rows = $db->resultset();
        $reply_count = $db->rowCount();
        return $reply_count;
    }

