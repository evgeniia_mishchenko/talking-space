<?php require('core/init.php'); ?>

<?php
    $topic = new Topic;
    $user = new User;
    $validate = new Validator;
    //this is the page that we submit our registration form to
    if(isset($_POST['register'])){  //our submit button on registration form has name="register", so we check if the submit button was clicked
        //Create Data Array(if true we want grap all fields from form)
        $data = array();
        $data['name'] = $_POST['name'];
        $data['email'] = $_POST['email'];
        $data['username'] = $_POST['username'];
        $data['password'] = md5($_POST['password']);    //to encrypt password
        $data['password2'] = md5($_POST['password2']);  //to encrypt password
        $data['about'] = $_POST['about'];
        $data['last_activity'] = date('Y-m-d H:i:s');   //current date

        //Required Fields
        $field_array = array('name', 'email', 'username', 'password', 'password2'); // should match our post names up here
        if($validate->isRequired($field_array)){
            if($validate->isValidEmail($data['email'])){
                if($validate->passwordsMatch($data['password'],$data['password2'])){
                    //if true it will move on and register the user:

                    //Upload Avatar Image
                    if($user->uploadAvatar()){
                        $data['avatar'] = $_FILES['avatar']['name'];
                    } else {
                        $data['avatar'] = 'avatar.jpg';
                    }

                    //And Register User
                    if ($user->register($data)){
                        redirect('index.php', 'You are registered and can now log in', 'success');
                    } else {
                        redirect('index.php', 'Something went wrong with registration.', 'error');
                    }

                } else {
                    redirect('register.php', 'Your passwords did not match', 'error');
                }
            } else {
                redirect('register.php', 'Please use a valid email address', 'error');
            }
        } else {
            redirect('register.php', 'Please fill in all required fields', 'error');
        }
    }

    $template = new Template('templates/register.php');
    echo $template;

//dont forget about total number of users to change

