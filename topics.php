<?php require('core/init.php'); ?>

<?php
    $user = new User();
    $topic = new Topic();
    //Get category from URL
    $category = isset($_GET['category']) ? $_GET['category'] : null;

    //Get user from URL
    $user_id = isset($_GET['user']) ? $_GET['user'] : null;

    $template = new Template('templates/topics.php');

    //Assign Template Vars
    //category filter
    if(isset($category)){
         $template->topics = $topic->getByCategory($category);
         $template->title = 'Posts In "'.$topic->getCategory($category)->name.'" ';
    }
    //user filter
    if(isset($user_id)){
        $template->topics = $topic->getByUser($user_id);
        $template->title = 'Posts In "'.$user->getUser($user_id)->username.'" ';
    }

    if(!isset($category) && !isset($user_id)){
        $template->topics = $topic->getAllTopics();
    }

    //For statistic area
    $template->totalUsers = $user->getTotalUsers();
    $template->totalTopics = $topic->getTotalTopics();
    $template->totalCategories = $topic->getTotalCategories();
    echo $template;


?>