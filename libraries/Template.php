<?php
/*
  * Template Class
  * Create a template/view object
  * Класс шаблонов это незаменимая часть в форуме, гостевой, чате и т.д.
  * Этот класс прост, причём очень прост.
  * Основные функции: загрузка шаблона, обработка в нём переменных и выход шаблона.
  */

    //Path to template
    //protected $template;
    //Variable passed in
    //protected $vars = array();
    //we want be able to set a value inside of our logic/controller or main file
    //and then pass that on to the template
    //this array allows us to pass data to a template whether it comes from a DB or we define it inside of a controller
    /*
     * Class Constructor
     */
    //public function __constructor($template){
    //    $this->template = $template;
    //}
    //whenever we create a new template class we gonna add a path to our template in the instantiation

    /*
     * Get template variables
     */
    //public function __get($key){
    //    return $this->vars[$key];
    //}
    //for reading the data from inaccessible properties
    /*
     * Set template variables
     */
    //public function __set($key, $value){
    //    $this->vars[$key] = $value;
    //}
    //set gonna run when we write data to inaccessible properties which we have up of class
    //so Get/Set allow us to pass in values into our template
    /*
     * Convert Object To String
     */
    //public function __toString(){
    //    extract($this->vars);   //to get our template vars
    //    chdir(dirname($this->template));
    //    ob_start(); //turn on output buffering, stored as an internal buffer

    //    include basename($this->template);  //to include template to our file
    //    return ob_get_clean();
    //}
    //lets us treate an Object as a string, and we can actually echo out our template
//this class is really simple but very reliable and easy to use
//next we will implement our template class inside index.php
//end before add a couple of lines to our initializer file which is
//in the core directory
//}

class Template {
    protected $template;
    protected $variables = array();

    public function __construct($template)    {
        $this->template = $template;
    }

    public function __get($key)    {
        return $this->variables[$key];
    }

    public function __set($key, $value)    {
        $this->variables[$key] = $value;
    }
    public function __toString()    {
        extract($this->variables);
        chdir(dirname($this->template));
        ob_start();
        include basename($this->template);
        return ob_get_clean();
    }
}
?>
