<?php

class Validator {
    /*
     *  Check Required Fields
     */
    public function isRequired($field_array){   //take an array of fields we want to be required
        foreach($field_array as $field){
            if($_POST[''.$field.''] == ''){
                return false;
            }
        }return true;
    }

    /*
     *  Validate email Field (we want server side validation as well )
     */
    public function isValidEmail($email){
        if(filter_var($email, FILTER_VALIDATE_EMAIL)){      //there are a lot of way: regular expr. are very popular but this is much easier
            return true;
        } else return false;
    }

    /*
     * Check (two) Password Match
     */
    public function passwordsMatch ($pw1, $pw2){
        if($pw1 == $pw2){
            return true;
        } else return false;
    }
}