<?php

    class Topic  {
        //Init DB Var
        private $db;
        /*
         * Constructor
         */
        public function __construct(){
            $this->db = new Database;
        }

        //For Topics Page/FrontPage
        /*
         * Get All Topics
         */
        public function getAllTopics (){
            $this->db->query("SELECT topics.*, users.username, users.avatar, categories.name FROM topics
                              INNER JOIN users ON topics.user_id = users.id
                              INNER JOIN categories ON topics.category_id = categories.id
                              ORDER BY create_date DESC");
            //Assign Result Set
            $results = $this->db->resultset();
            return $results;
        }

        /*
         * Filter topics by category
         */
        public function getByCategory($category_id){
            $this->db->query("SELECT topics.*, users.username, users.avatar, categories.* FROM topics
                              INNER JOIN users ON topics.user_id = users.id
                              INNER JOIN categories ON topics.category_id = categories.id
                              WHERE topics.category_id = :category_id
                              ORDER BY create_date DESC");
            $this->db->bind(':category_id', $category_id);
            //Assign row
            $results = $this->db->resultset();
            return $results;
        }

        /*
         * Filter topics by user
         */
        public function getByUser($user_id){
            $this->db->query("SELECT topics.*, users.username, users.avatar, categories.* FROM topics
                              INNER JOIN users ON topics.user_id = users.id
                              INNER JOIN categories ON topics.category_id = categories.id
                              WHERE topics.user_id = :user_id
                              ORDER BY create_date DESC");
            $this->db->bind(':user_id', $user_id);
            //Assign row
            $results = $this->db->resultset();
            return $results;
        }

        /*
         * Get By Category ID
         */

        public function getCategory($category_id){
            $this->db->query('SELECT * FROM categories WHERE id = :category_id');
            $this->db->bind(':category_id', $category_id);
            //Assign row
            $row = $this->db->single();
            return $row;
        }

        //Statistic Area

        /*
         * Get Total Number of Users


        public function getTotalUsers(){
            $this->db->query('SELECT * FROM users');
            $this->db->resultset();
            return $this->db->rowCount();
        }*/

        /*
         * Get Total Number of Topics
         */

        public function getTotalTopics(){
            $this->db->query('SELECT * FROM topics');
            $rows = $this->db->resultset();
            return $this->db->rowCount();
        }

        /*
         * Get Total Number of Categories
         */

        public function getTotalCategories(){
            $this->db->query('SELECT * FROM categories');
            $rows = $this->db->resultset();
            return $this->db->rowCount();
        }

        //For Single Topic Page
        /*
         * Get Topic By ID
         */
        public function getTopic ($id) {
            $this->db->query('SELECT topics.*, users.username, users.name, users.avatar, categories.name FROM topics
                                INNER JOIN users ON topics.user_id = users.id
                                INNER JOIN categories ON topics.category_id = categories.id
                                WHERE topics.id = :id');
            $this->db->bind(':id', $id);
            $row = $this->db->single();
            return $row;
        }

        /*
         * Get Topic Replies
         */
        public function getReplies($topic_id){
            $this->db->query('SELECT replies.*, users.* FROM replies
                                INNER JOIN users ON replies.user_id = users.id
                                WHERE replies.topic_id = :topic_id
                                ORDER BY create_date ASC');
            $this->db->bind(':topic_id', $topic_id);
            $result = $this->db->resultset();
            return $result;
        }

        /*
         *Create Topic
         */
        public function create($data){
            $this->db->query('INSERT INTO topics (category_id, user_id, title, body)
                                            VALUES (:category_id, :user_id, :title, :body)');
            $this->db->bind(':category_id', $data['category_id']);
            $this->db->bind(':user_id', $data['user_id']);
            $this->db->bind(':title', $data['title']);
            $this->db->bind(':body', $data['body']);
            if($this->db->execute()){
               return true;
            } else {
                return false;
            }
        }

        /*
         * Add Reply
         */
        public function reply($data){
            $this->db->query('INSERT INTO replies (topic_id, user_id, body)
                                          VALUES (:topic_id, :user_id, :body)');
            $this->db->bind(':topic_id', $data['topic_id']);
            $this->db->bind(':user_id', $data['user_id']);
            $this->db->bind(':body', $data['body']);
            if($this->db->execute()){
                return true;
            } else {
                return false;
            }
        }



    }

?>