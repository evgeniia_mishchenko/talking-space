<?php
class User {

    //Init DB Var
    private $db;
    /*
     * Constructor
     */
    public function __construct(){
        $this->db = new Database;
    }

    /*
     * Get Total Number of Users(for Statistic area)
     */

    public function getTotalUsers(){
        $this->db->query('SELECT * FROM users');
        $this->db->resultset();
        return $this->db->rowCount();
    }


    /*
         * Get By User ID
         */

    public function getUser($user_id){
        $this->db->query('SELECT * FROM users WHERE id = :user_id');
        $this->db->bind(':user_id', $user_id);
        $row = $this->db->single();
        return $row;
    }

    /*
     * Upload Avatar
     */
    public function uploadAvatar (){
        $allowedExts = array('gif', 'jpeg', 'jpg', 'png');
        $temp = explode('.', $_FILES['avatar']['name']);  //explode() - Разбивает строку на подстроки, return array where last element will by extention
        $extension = end($temp);    // устанавливает внутренний указатель array на последний элемент и возвращает его значение.
                                    /*
                                     * $fruits = array('apple', 'banana', 'cranberry');
                                     * echo end($fruits); // cranberry
                                     */
        if ((($_FILES['avatar']['type'] == 'image/gif')
                || ($_FILES['avatar']['type'] == 'image/jpeg')
                || ($_FILES['avatar']['type'] == 'image/jpg')
                || ($_FILES['avatar']['type'] == 'image/pjpeg')
                || ($_FILES['avatar']['type'] == 'image/x-png')
                || ($_FILES['avatar']['type'] == 'image/png'))
                && ($_FILES['avatar']['size'] < 50000)  //img less then 5kb
                && in_array($extension, $allowedExts)){
            if($_FILES['avatar']['error'] > 0){
                redirect('register.php', $_FILES['avatar']['error'], 'error');  //redirect our custom function. it is often part of frameworks
                //but we also need to catch our messages, just yet we sending it but have nothing to output it(in header with help of system_helper(displayMessage function)
                //the error message should be stored in the session so we should be able to
            }else {
                if(file_exists('images/avatars/' . $_FILES['avatar']['name'])){
                    redirect('register.php', 'File already exists!', 'error');
                } else {
                    move_uploaded_file($_FILES['avatar']['tmp_name'],'images/avatars/' . $_FILES['avatar']['name']);
                    return true;
                }
            }
        } else {
            redirect('register.php', 'Invalid file type!', 'error');
        }

    }

    /*
     * Register User
     */
    public function register ($data){
        //Insert Query
        $this->db->query('INSERT INTO users (name, email, username, password, avatar, about, last_activity)
                            VALUES (:name, :email, :username, :password, :avatar, :about, :last_activity)');
        $this->db->bind(':name', $data['name']);
        $this->db->bind(':email', $data['email']);
        $this->db->bind(':username', $data['username']);
        $this->db->bind(':password', $data['password']);
        $this->db->bind(':avatar', $data['avatar']);
        $this->db->bind(':about', $data['about']);
        $this->db->bind(':last_activity', $data['last_activity']);
        //Execute(place inside of DB)
        if($this->db->execute()){   //if OK returns true(which will go to register.php controller in Register user area from where we will get message about success/failure registration )
            return true;
        } else return false;
    }



    /*
	 * User Login
	 */

    public function login($username, $password){
        $this->db->query("SELECT * FROM users
									WHERE username = :username
									AND password = :password
		");

        //Bind Values
        $this->db->bind(':username', $username);
        $this->db->bind(':password', $password);

        $row = $this->db->single();  //because we are only looking for one record
        //Check Rows
        if($this->db->rowCount() > 0){  //which means a row came back - we return true but also run setUserData method
            $this->setUserData($row);
            return true;
        } else {
            return false;
        }
    }
    //setUserData method sets some session vars for us which we can use anywhere in our script in any time(for Login form)
    //to hold one boolean expression for isLoggedIn because we want to know if user is logged in or not
    //we also gonna store the user id/name/username

    /*
     * Set User Data
     */
    private function setUserData($row){
        $_SESSION['is_logged_in'] = true;
        $_SESSION['user_id'] = $row->id;
        $_SESSION['username'] = $row->username;
        $_SESSION['name'] = $row->name;
    }

    /*
     * User Logout
     */
    public function logout(){
        unset($_SESSION['is_logged_in']);
        unset($_SESSION['user_id']);
        unset($_SESSION['username']);
        unset($_SESSION['name']);
        return true;
    }
}

?>