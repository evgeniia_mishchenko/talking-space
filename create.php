<?php require('core/init.php'); ?>

<?php
    $topic = new Topic;

    //to see if do_create button is clicked
    if(isset($_POST['do_create'])){
        //we need validation here
        $validate = new Validator;
        //Create data array
        $data = array();
        $data['title'] = $_POST['title'];
        $data['category_id'] = $_POST['category'];
        $data['body'] = $_POST['body'];
        $data['user_id'] = getUser()['user_id'];    //logged in user's info
        $data['last_activity'] = date('Y-m-d H:i:s');

        //Required Fields
        $field_array = array('title', 'body', 'category');
        if($validate->isRequired($field_array)){
            if($topic->create($data)){
                    redirect('index.php', 'Your topic has been posted', 'success');
                } else {
                redirect('topic.php?id='.$topic_id, 'Something went wrong with your post', 'error');
            }
        } else {
            redirect('create.php', 'Please fill in all required fields', 'error');
        }
    }
    $template = new Template('templates/create.php');
    echo $template;
?>